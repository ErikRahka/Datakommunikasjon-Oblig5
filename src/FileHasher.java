import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * This FileHasher class uses {@link MessageDigest} to hash or verify the contents of
 * files using a valid hash algorithm (see {@link MessageDigest} for valid algorithms).
 *
 * The FileHasher class only hashes {@File} objects, the hash is stored in a {@link byte} array.
 */

public class FileHasher {

    private MessageDigest digest;

    /**
     * Constructs a new FileHasher object that uses a {@link MessageDigest} object that
     * hashes the content of files using the supplied algorithm.
     *
     * @throws NoSuchAlgorithmException if the supplied algorithm could not be found
     *
     * @param algorithm the hash algrithm to use (see {@link MessageDigest} for valid algorithms)
     */
    public FileHasher(String algorithm) throws NoSuchAlgorithmException {
        digest = MessageDigest.getInstance(algorithm);
    }

    /**
     * Hashes the contents of a file and returns the hash in a {@link byte} array.
     *
     *
     * @param file the file to be hashed
     *
     * @throws IOException if something went wrong when reading the file
     *
     * @return the hash of the file content
     */
    public byte[] hash(File file) throws IOException {
        return hash(new FileInputStream(file));
    }

    /**
     * Returns wether or not the supplied hash matches the hash of the contents of the supplied file.
     *
     * @param hash the hash to check
     * @param file the file to check against
     *
     * @throws IOException if something went wrong when reading the file
     *
     * @return wether or not hash matches the hash of the contents of file
     */
    public boolean verify(byte[] hash, File file) throws IOException {
        return Arrays.equals(hash, hash(new FileInputStream(file)));
    }

    /**
     * Hashes the contents of the InputStream and returns that hash. Feeds the digest object with 1024 byte
     * from the InputStream at a time, and return the hash of this data when the InputStream is empty.
     *
     * @param input the InputStream from which to read
     *
     * @throws IOException if the InputStream cannot be read
     *
     * @return the hash of the data contained in input
     */
    protected byte[] hash(InputStream input) throws IOException {
        byte[] bytes = new byte[1024];
        int len;
        while ((len = input.read(bytes, 0, 1024)) > -1) {
            digest.update(bytes, 0, len);
        }

        return digest.digest();
    }

}
