import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * This FileIterator class provides a method of iterating through a directory
 * and all its sub-directories.
 *
 * A FileIterator class must be constructed with a file that points to a directory.
 * It recursively adds any files it finds into a {@link LinkedList}. Using the {@link FileIterator#next()}
 * method removes and returns the first file that was added into the linked list. A consequence of this
 * is that files that are found closer to the root directory are returned first.
 *
 * If the FileIterator encounters a directory it does not have access to it will simply ignore the diretory.
 * If the directory passed in the constructor cannot be read then the FileIterator will be empty.
 */

public class FileIterator implements Iterator<File> {

    private LinkedList<File> files;

    /**
     * Creates a FileIterator that iterates through any files that are contained in
     * root or any of its sub-directories. Recursively adds any files that are found
     * in root or any of its sub-directories into the files LinkedList. If any files are
     * added into root or any of its sub-directories after the construction of this
     * FileIterator then they will not be iterated through by this object. If root cannot be
     * read then the FileIterator will be empty.
     *
     * @param root the directory to iterate through
     */
    public FileIterator(File root) {
        if (!root.isDirectory()) {
            throw new IllegalArgumentException("Root is not a directory!");
        }

        this.files = new LinkedList<>();
        iterate(this.files, root);
    }

    /**
     * Recursive method to iterate through a directories files and its sub-directories
     * and adds them into the files LinkedList
     *
     * @param files in which the files are added
     * @param dir which directory to search through
     */
    private void iterate(LinkedList<File> files, File dir) {
        File[] listFiles = dir.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    iterate(files, file);
                } else {
                    files.addLast(file);
                }
            }
        }
    }

    @Override
    public boolean hasNext() {
        return !files.isEmpty();
    }

    @Override
    public File next() {
        if (!hasNext()) {throw new NoSuchElementException("next is null");}

        return files.pollFirst();
    }

}
