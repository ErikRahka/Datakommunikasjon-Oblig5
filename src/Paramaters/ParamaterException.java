package Paramaters;

/**
 * An exception used to indicate that there was an invalid argument supplied to a {@link ParameterInterpreter}
 */
public class ParamaterException extends RuntimeException {

	public ParamaterException(String string) {
		super(string);
	}

	public ParamaterException(String string, Object ... args) {
		super(String.format(string,args));
	}

}
