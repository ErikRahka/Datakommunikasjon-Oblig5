import java.io.*;
import java.nio.file.Files;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/**
 * This class signs and verifies files. It requires a KeyStore and its password.
 * It uses SHA1withRSA to sign and verify, this cannot be changed.
 */

public class FileSigner {

    private static final String algorithm = "SHA1withRSA";

    private KeyStore keyStore;
    private char[] password;

    public FileSigner(KeyStore keyStore, char[] password) {
        this.keyStore = keyStore;
        this.password = password;
    }

    /**
     * Signs a file with the key denoted by the alias and saves the signature of its contents to another files.
     *
     * @param input the file we want to sign
     * @param output the file to which we want to save the signature
     * @param alias the alias of the key in the keystore
     */
    public void sign(File input, File output, String alias) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, InvalidKeyException, IOException, SignatureException {

        //Fetch privatekey from keystore
        PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, password);

        //Create signature with algorithm
        Signature signature = Signature.getInstance(algorithm);

        //Init
        signature.initSign(privateKey);

        //Update data 1024 bytes at a time
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(input));
        byte[] buffer = new byte[1024];
        int len;
        while ((len = inputStream.read(buffer)) >= 0) {
            signature.update(buffer, 0, len);
        }

        //Sign data
        byte[] signedData = signature.sign();

        //Write the signature to output
        Files.write(output.toPath(), signedData);

    }

    /**
     * Verifies that a file and a signature matches with the key denoted by the alias.
     *
     * @param target the file we want to verify
     * @param signatureFile the signature we want to verify
     * @param alias the alias of the key within the keystore
     *
     * @return if the signature matches or not
     */
    public boolean verify(File target, File signatureFile, String alias) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException, CertificateException, KeyStoreException {

        //Get the certificate we want to verify with
        Certificate certificate = getCertificateFromKeyStore(keyStore, alias);

        //Get public key from certificate
        PublicKey publicKey = certificate.getPublicKey();

        //Create signature object
        Signature signature = Signature.getInstance(algorithm);

        //Supply public key to signature object
        signature.initVerify(publicKey);

        //Update data 1024 bytes at a time
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(target));
        byte[] buffer = new byte[1024];
        int len;
        while ((len = inputStream.read(buffer)) >= 0) {
            signature.update(buffer, 0, len);
        }

        //Verify file with the signature file
        return signature.verify(Files.readAllBytes(signatureFile.toPath()));
    }

    /**
     * Fetches a certificate from the keystore
     *
     * @param keystore the keystore from which we want to fetch
     * @param alias the certificates alias within the keystore
     *
     * @throws Exception if anything went wrong
     *
     * @return the certifiate fetched
     */
    private static Certificate getCertificateFromKeyStore(KeyStore keystore, String alias) throws KeyStoreException, CertificateException {
        if (keystore.containsAlias(alias)) {
            Certificate certificate = keystore.getCertificate(alias);
            return certificate;
        } else {
            throw new CertificateException("Could not find certificate with alias \"" + alias + "\" in keystore");
        }
    }

}
