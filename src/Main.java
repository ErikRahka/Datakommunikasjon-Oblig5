import Paramaters.*;

import java.io.*;
import java.security.KeyStore;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Main {

    static boolean debug;

    public static void main(String[] args) {
        ParameterInterpreter interpreter = new ParameterInterpreter(
                new FunctionFlag("target", "t", "Specify a file/directory to verify", Main::getTargetFile),
                new FunctionFlag("hashstore", "hs", "Use a specific hashstore. Default: <hashstore> in running directory", Main::getHashStoreFile),
                new Flag("rehash", "re", "Update the hashstore after verifying"),
                new Flag("debug", "d", "Prints stacktrace if an exception occurs")
        );
        ParameterInterpretation arguments = interpreter.intepret(args);

        try {

            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(Main.class.getResourceAsStream("keystore"), "password".toCharArray());

            //Set target file/directory to "target" argument, or current working folder if no "target" argument was supplied
            File target = (File) arguments.get("target", new File("."));

            //Set debug to true if the debug flag is present
            debug = arguments.has("debug");

            //Set hashstore file to "hashstore" argument, or "hashstore" in current working directory if no "hashstore" argument was supplied
            File hashStoreFile = (File) arguments.get("hashstore", new File("hashstore"));
            File hashStoreSignatureFile = new File(hashStoreFile.toString() + ".signature");

            FileSigner fileSigner = new FileSigner(keyStore, "password".toCharArray());

            HashStore<File> hashStore;
            if (hashStoreFile.exists() && hashStoreSignatureFile.exists()) {
                if (fileSigner.verify(hashStoreFile, hashStoreSignatureFile, "filetool")) {
                    System.out.println("Signature was verified, reading HashStore from \"" + hashStoreFile + "\"");
                    hashStore = (HashStore<File>) readObject(hashStoreFile);
                } else {
                    System.out.println("Signature for hashstore was not verified!");
                    return;
                }
            } else if (hashStoreFile.exists()) {
                System.out.println("Signature file for hashtore could not be found!");
                return;
            } else {
                hashStore = new HashStore<>();
            }

            FileHasher hasher = new FileHasher("SHA-256");
            if (target.isDirectory()) {
                HashStore<File> updatedHashStore = hashDirectory(target, hasher);

                //Compare the current HashStore with the old one
                HashMap<File, HashStore.Difference> difference = hashStore.compare(updatedHashStore);

                //If the number of unchanged files equals the size of the HashMap, then no changes have been found
                boolean noChanges = difference.values().stream().filter(HashStore.Difference.UNCHANGED::equals).count() == difference.size();

                if (!noChanges) {
                    long changed = difference.values().stream().filter(HashStore.Difference.CHANGED::equals).count();
                    long added = difference.values().stream().filter(HashStore.Difference.NEW::equals).count();
                    long deleted = difference.values().stream().filter(HashStore.Difference.DELETED::equals).count();

                    System.out.println("Iterated through " + difference.size() + " files:");
                    System.out.printf("\t%d changed files, %d deleted files, %d new files\n", changed, deleted, added);
                    difference.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).forEach((Map.Entry<File, HashStore.Difference> entry) -> printFileStatus(entry.getKey(), entry.getValue()));
                } else {
                    System.out.println("Iterated through " + difference.size() + " files:");
                    System.out.println("No changes have been made to any files or directories");
                }

                if (!hashStoreFile.exists() || arguments.has("rehash")) {
                    boolean update = true;
                    if (hashStoreFile.exists()) {
                        if (!noChanges) {
                            System.out.println("Updating HashStore at \"" + hashStoreFile + "\"");
                        } else {
                            System.out.println("Nothing to update the HashStore with");
                            update = false;
                        }
                    } else {
                        System.out.println("Creating new HashStore at \"" + hashStoreFile + "\"");
                    }

                    if (update) {
                        writeObject(hashStoreFile, updatedHashStore);
                        fileSigner.sign(hashStoreFile, hashStoreSignatureFile, "filetool");
                    }
                }
            } else {
                byte[] hash = hasher.hash(target);

                HashStore.Difference difference = hashStore.compare(hash, target);
                System.out.printf("Checking file at \"%s\" against HashStore at \"%s\"\n", target, hashStoreFile);
                printFileStatus(target, difference);
                if (difference == HashStore.Difference.UNCHANGED) {
                    System.out.println("\tunchanged file:\t"+target);
                }

                if (!hashStoreFile.exists() || arguments.has("rehash")) {
                    boolean update = true;
                    if (hashStoreFile.exists()) {
                        if (difference != HashStore.Difference.UNCHANGED) {
                            System.out.println("Updating HashStore at \"" + hashStoreFile + "\"");
                        } else {
                            update = false;
                            System.out.println("Not updating HashStore, target is unchanged.");
                        }
                    } else {
                        System.out.println("Creating new HashStore at \"" + hashStoreFile + "\"");
                    }

                    if (update) {
                        hashStore.update(hash, target);

                        writeObject(hashStoreFile, hashStore);
                        fileSigner.sign(hashStoreFile, hashStoreSignatureFile, "filetool");
                    }
                }
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    /**
     * Prints if the file has been changed, if it has been deleted, or
     * if it is a new.
     *
     * @param file the file for which the difference applies to
     * @param difference the difference of the file
     */
    public static void printFileStatus(File file, HashStore.Difference difference) {
        switch (difference) {
            case CHANGED: System.out.println("\tchanged file:\t"+file); break;
            case NEW: System.out.println("\tnew file:\t"+file); break;
            case DELETED: System.out.println("\tdeleted file:\t"+file); break;
            case UNCHANGED: break;
        }
    }

    /**
     * Iterates through a directory's (and its sub-directories) files and creates
     * a new HashStore which stores the files and their hashes.
     *
     * @param directory a file that is a directory
     * @param hasher used to hash the files
     *
     * @return a HashStore object containing all of the directories (and sub-directories) files and their hashes
     */
    public static HashStore<File> hashDirectory(File directory, FileHasher hasher) {
        HashStore<File> hashstore = new HashStore<>();

        FileIterator iterator = new FileIterator(directory);
        while (iterator.hasNext()) {
            File file = iterator.next();
            try {
                byte[] hash = hasher.hash(file);
                hashstore.update(hash, file);
            } catch (IOException e) {
                System.out.println("Ignoring file at \"" + file + "\". Failed to read.");
            }
        }

        return hashstore;
    }

    /**
     * Handles the "target" argument if it is supplied. It creates a file
     * from the "target" argument, if it does not exists or we cannot read from it then
     * it will throw a {@link ParamaterException}, which results in termination of the program.
     *
     * @param targetPath the "target" argument
     *
     * @return File of the target
     */
    public static File getTargetFile(String targetPath) {
        File targetFile = new File(targetPath);
        if (!targetFile.exists()) {
            throw new ParamaterException("Target file does not exist!");
        } else if (!targetFile.canRead()) {
            throw new ParamaterException("Cannot read from target file!");
        }
        return targetFile;
    }

    /**
     * Handles the "hashstore" argument if it is supplied. It creates a file
     * from the "hashstore" argument. Throws a {@link ParamaterException}
     * if the file exists and cannot read or write from/to the file, which results in termination of the program.
     *
     * @param hashStorePath the "hashstore" argument
     *
     * @return File of the hashstore file
     */
    public static File getHashStoreFile(String hashStorePath) {
        File hashStoreFile = new File(hashStorePath);
        if (hashStoreFile.exists() && !hashStoreFile.canRead()) {
            throw new ParamaterException("Cannot read from hashstore file!");
        } else if (hashStoreFile.exists() && !hashStoreFile.canWrite()) {
            throw new ParamaterException("Cannot write to hashstore file!");
        }
        return hashStoreFile;
    }

    /**
     * Writes an Object to a file using {@link ObjectOutputStream}.
     *
     * @throws IOException if something went wrong when writing the file
     *
     * @param file in which to store the object in
     */
    public static void writeObject(File file, Object object) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));

        outputStream.writeObject(object);
    }

    /**
     * Reads an Object from a file using {@link ObjectInputStream}.
     * Returns null if the file does not exists.
     *
     * @param file where an object is stored
     *
     * @throws IOException if something went wrong when reading the file
     *
     * @return the object read from the file
     */
    public static Object readObject(File file) throws ClassNotFoundException, IOException {
        if (file.exists()) {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));
            return input.readObject();
        }
        return null;
    }

    /**
     * Prints only the message of the exception unless the "debug"
     * flag has been set. If the "debug" flag has been set
     * then it will print the stacktrace of the exception
     * using the {@link Exception#printStackTrace()} method.
     *
     * @param e the exception to be handled
     */
    public static void handleException(Exception e) {
        if (debug) {
            e.printStackTrace();
        } else {
            System.out.println(e.getMessage());
        }
    }

}
