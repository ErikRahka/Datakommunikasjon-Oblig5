import java.io.Serializable;
import java.util.*;

/**
 * This class stores objects and their corresponding hashes. It provides
 * methods for adding and removing objects, as well as methods to compare
 * HashStores.
 *
 * It uses a HashMap to retain the relastionship between an object an its hash.
 */

public class HashStore<T> implements Serializable {

    enum Difference {
        UNCHANGED,
        CHANGED,
        DELETED,
        NEW
    }

    private HashMap<T, ByteArray> paths;

    /**
     * Creates a new empty HashStore
     */
    public HashStore() {
        paths = new HashMap<>();
    }

    /**
     * Adds an object and its hash to the HashStore
     *
     * @param hash the hash of t
     * @param t the object we want to store
     */
    public void update(byte[] hash, T t) {
        paths.put(t, new ByteArray(hash));
    }

    /**
     * Checks if an object is stored in this HashStore
     *
     * @param t the object we want to check
     * @return wether or not the hashstore contains the object
     */
    public boolean contains(T t) {
        return paths.containsKey(t);
    }

    /**
     * Returns the hash of an object that is stored in this hashstore. Will
     * throw a NullPointerException if the HashMap dows not contain the object.
     * Use {@link #contains} before calling this method.
     *
     * @param t the object for which we want to retrieve the correspnding hash
     * @return the hash of the object
     */
    public byte[] get(T t) {
        return paths.get(t).array;
    }

    /**
     * Compares a object and its hash to find if the object is already stored
     * in the hash, if its hash has been changed, or if nothing has changed.
     *
     * See {@link Difference}.
     *
     * @param hash the hash of t
     * @param t the object we want to compare
     *
     * @return the difference
     */
    public Difference compare(byte[] hash, T t) {
        if (contains(t)) {
            if (!paths.get(t).equals(new ByteArray(hash))) {
                return Difference.CHANGED;
            }
        } else {
            return Difference.NEW;
        }
        return Difference.UNCHANGED;
    }

    /**
     * Compares a HashStore to another Hashstore. Loops through the supplied HashStore's
     * objects and hashes and compares them to the ones stored in this HashStore. It uses
     * the {@link #compare(byte[], Object)} method to compare the individual objects and hashes.
     *
     * It also checks if the supplied HashStore is missing any objects that this HashStore has, if
     * it does, then it will mark those objects as {@link Difference#DELETED}, to signal that
     * an object is missing.
     *
     * @param hashStore the HashStore we want to compare
     *
     * @return HashMap of objects and their difference
     */
    public HashMap<T, Difference> compare(HashStore<T> hashStore) {
        HashMap<T, Difference> difference = new HashMap<>();

        HashMap<T, ByteArray> absentPaths = (HashMap<T, ByteArray>) paths.clone();

        for (Map.Entry<T, ByteArray> entry : hashStore.paths.entrySet()) { ;
            T t = entry.getKey();
            byte[] hash = entry.getValue().array;

            absentPaths.remove(t, hash);
            difference.put(t, compare(hash, t));
         }

        for (Map.Entry<T, ByteArray> entry : absentPaths.entrySet()) {
            difference.putIfAbsent(entry.getKey(), Difference.DELETED);
        }

        return difference;
    }

    /**
     * This byte[] wrapper provides a proper hashCode and equals method that the
     * HashMap requires to compare its values.
     */
    private static class ByteArray implements Serializable {

        byte[] array;

        public ByteArray(byte[] array) {
            this.array = array;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ByteArray) {
                return Arrays.equals(array, ((ByteArray) obj).array);
            }
            return super.equals(obj);
        }

    }

}
